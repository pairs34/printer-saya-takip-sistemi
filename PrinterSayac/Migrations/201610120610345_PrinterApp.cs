namespace PrinterSayac.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrinterApp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Counters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PrinterID = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        IsActive = c.Int(nullable: false),
                        CounterDate = c.DateTime(nullable: false, precision: 0),
                        Description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Printers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PrinterName = c.String(unicode: false),
                        IpAdress = c.String(unicode: false),
                        Location = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Printers");
            DropTable("dbo.Counters");
        }
    }
}
