﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterSayac.Entities
{
    public class Counter
    {
        [Key]
        public int ID { get; set; }
        public int PrinterID { get; set; }
        public int StartCount { get; set; }
        public int EndCount { get; set; }
        public int IsActive { get; set; }
        public DateTime CounterDate { get; set; }
        public string Description { get; set; }
    }
}
