﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterSayac.Entities
{
    public class DbHelper : DbContext
    {
        static DbHelper()
        {
            DbConfiguration.SetConfiguration(new MySql.Data.Entity.MySqlEFConfiguration());
        }
        public DbSet<Counter> Counters { get; set; }
        public DbSet<Printer> Printers { get; set; }
    }
}
