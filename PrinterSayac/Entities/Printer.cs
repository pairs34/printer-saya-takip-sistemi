﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterSayac.Entities
{
    public class Printer
    {
        [Key]
        public int ID { get; set; }

        public string PrinterName { get; set; }

        public string IpAdress { get; set; }

        public string Location { get; set; }
    }
}
