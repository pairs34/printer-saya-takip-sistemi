﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterSayac
{
    public static class Helpers
    {
        public static DateTime PreviousMonthFirstDay(this DateTime currentDate)
        {
            DateTime d = currentDate.PreviousMonthLastDay();

            return new DateTime(d.Year, d.Month, 1);
        }

        public static DateTime PreviousMonthLastDay(this DateTime currentDate)
        {
            return new DateTime(currentDate.Year, currentDate.Month, 1).AddDays(-1);
        }

        public static DateTime FirstDayOfMonth(this DateTime value)
        {
            return value.Date.AddDays(1 - value.Day);
        }

        public static DateTime LastDayOfMonth(this DateTime value)
        {
            return value.FirstDayOfMonth().AddMonths(1).AddDays(-1);
        }
    }
}
