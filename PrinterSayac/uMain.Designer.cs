﻿namespace PrinterSayac
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if (--OpenFormCount == 0) System.Windows.Forms.Application.Exit();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tabController = new DevExpress.XtraBars.TabFormControl();
            this.themeButton = new DevExpress.XtraBars.SkinBarSubItem();
            this.tabManager = new DevExpress.XtraBars.TabFormDefaultManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.mainTab = new DevExpress.XtraBars.TabFormPage();
            this.mainScControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupAddCounter = new DevExpress.XtraEditors.GroupControl();
            this.txtStartCounter = new DevExpress.XtraEditors.TextEdit();
            this.lblStartCounter = new DevExpress.XtraEditors.LabelControl();
            this.lstPrinterNames = new DevExpress.XtraEditors.LookUpEdit();
            this.btnAddCounter = new DevExpress.XtraEditors.SimpleButton();
            this.txtDescription = new DevExpress.XtraEditors.TextEdit();
            this.lblDescription = new DevExpress.XtraEditors.LabelControl();
            this.txtCounter = new DevExpress.XtraEditors.TextEdit();
            this.lblCounter = new DevExpress.XtraEditors.LabelControl();
            this.lblPrinterNames = new DevExpress.XtraEditors.LabelControl();
            this.groupAddPrinter = new DevExpress.XtraEditors.GroupControl();
            this.btnPrinterAdd = new DevExpress.XtraEditors.SimpleButton();
            this.txtPrinterLocation = new DevExpress.XtraEditors.TextEdit();
            this.lblPrinterLocation = new DevExpress.XtraEditors.LabelControl();
            this.txtPrinterIP = new DevExpress.XtraEditors.TextEdit();
            this.lblPrinterIP = new DevExpress.XtraEditors.LabelControl();
            this.txtPrinterName = new DevExpress.XtraEditors.TextEdit();
            this.lblPrinterName = new DevExpress.XtraEditors.LabelControl();
            this.actionMenu = new DevExpress.XtraNavBar.NavBarControl();
            this.otherActions = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnPrinterList = new DevExpress.XtraNavBar.NavBarItem();
            this.btnCounterList = new DevExpress.XtraNavBar.NavBarItem();
            ((System.ComponentModel.ISupportInitialize)(this.tabController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabManager)).BeginInit();
            this.mainScControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupAddCounter)).BeginInit();
            this.groupAddCounter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartCounter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstPrinterNames.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCounter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAddPrinter)).BeginInit();
            this.groupAddPrinter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinterLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinterIP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinterName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actionMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // tabController
            // 
            this.tabController.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.themeButton});
            this.tabController.LinkProvider.TitleItemLinks.Add(this.themeButton);
            this.tabController.Location = new System.Drawing.Point(0, 0);
            this.tabController.Manager = this.tabManager;
            this.tabController.Name = "tabController";
            this.tabController.Pages.Add(this.mainTab);
            this.tabController.SelectedPage = this.mainTab;
            this.tabController.ShowAddPageButton = false;
            this.tabController.Size = new System.Drawing.Size(590, 50);
            this.tabController.TabForm = this;
            this.tabController.TabIndex = 0;
            this.tabController.TabStop = false;
            this.tabController.PageCreated += new DevExpress.XtraBars.PageCreatedEventHandler(this.tabController_PageCreated);
            this.tabController.PageClosing += new DevExpress.XtraBars.PageClosingEventHandler(this.tabController_PageClosing);
            this.tabController.OuterFormCreating += new DevExpress.XtraBars.OuterFormCreatingEventHandler(this.OnOuterFormCreating);
            // 
            // themeButton
            // 
            this.themeButton.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.themeButton.Caption = "Temalar";
            this.themeButton.Glyph = ((System.Drawing.Image)(resources.GetObject("themeButton.Glyph")));
            this.themeButton.GlyphDisabled = ((System.Drawing.Image)(resources.GetObject("themeButton.GlyphDisabled")));
            this.themeButton.Id = 0;
            this.themeButton.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("themeButton.LargeGlyph")));
            this.themeButton.LargeGlyphDisabled = ((System.Drawing.Image)(resources.GetObject("themeButton.LargeGlyphDisabled")));
            this.themeButton.Name = "themeButton";
            // 
            // tabManager
            // 
            this.tabManager.DockControls.Add(this.barDockControlTop);
            this.tabManager.DockControls.Add(this.barDockControlBottom);
            this.tabManager.DockControls.Add(this.barDockControlLeft);
            this.tabManager.DockControls.Add(this.barDockControlRight);
            this.tabManager.Form = this;
            this.tabManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.themeButton});
            this.tabManager.MaxItemId = 1;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(590, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 464);
            this.barDockControlBottom.Size = new System.Drawing.Size(590, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 464);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(590, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 464);
            // 
            // mainTab
            // 
            this.mainTab.ContentContainer = this.mainScControl;
            this.mainTab.Image = ((System.Drawing.Image)(resources.GetObject("mainTab.Image")));
            this.mainTab.Name = "mainTab";
            this.mainTab.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            this.mainTab.Text = "Anasayfa";
            // 
            // mainScControl
            // 
            this.mainScControl.Controls.Add(this.panelControl1);
            this.mainScControl.Controls.Add(this.actionMenu);
            this.mainScControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainScControl.Location = new System.Drawing.Point(0, 50);
            this.mainScControl.Name = "mainScControl";
            this.mainScControl.Size = new System.Drawing.Size(590, 414);
            this.mainScControl.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelControl1.Controls.Add(this.groupAddCounter);
            this.panelControl1.Controls.Add(this.groupAddPrinter);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(364, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(226, 414);
            this.panelControl1.TabIndex = 5;
            // 
            // groupAddCounter
            // 
            this.groupAddCounter.CaptionImage = ((System.Drawing.Image)(resources.GetObject("groupAddCounter.CaptionImage")));
            this.groupAddCounter.Controls.Add(this.txtStartCounter);
            this.groupAddCounter.Controls.Add(this.lblStartCounter);
            this.groupAddCounter.Controls.Add(this.lstPrinterNames);
            this.groupAddCounter.Controls.Add(this.btnAddCounter);
            this.groupAddCounter.Controls.Add(this.txtDescription);
            this.groupAddCounter.Controls.Add(this.lblDescription);
            this.groupAddCounter.Controls.Add(this.txtCounter);
            this.groupAddCounter.Controls.Add(this.lblCounter);
            this.groupAddCounter.Controls.Add(this.lblPrinterNames);
            this.groupAddCounter.Location = new System.Drawing.Point(5, 183);
            this.groupAddCounter.Name = "groupAddCounter";
            this.groupAddCounter.Size = new System.Drawing.Size(214, 194);
            this.groupAddCounter.TabIndex = 5;
            this.groupAddCounter.Text = "Sayaç Ekle";
            // 
            // txtStartCounter
            // 
            this.txtStartCounter.EditValue = "";
            this.txtStartCounter.EnterMoveNextControl = true;
            this.txtStartCounter.Location = new System.Drawing.Point(64, 68);
            this.txtStartCounter.MenuManager = this.tabManager;
            this.txtStartCounter.Name = "txtStartCounter";
            this.txtStartCounter.Properties.Mask.EditMask = "d";
            this.txtStartCounter.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtStartCounter.Size = new System.Drawing.Size(136, 20);
            this.txtStartCounter.TabIndex = 14;
            // 
            // lblStartCounter
            // 
            this.lblStartCounter.Location = new System.Drawing.Point(8, 71);
            this.lblStartCounter.Name = "lblStartCounter";
            this.lblStartCounter.Size = new System.Drawing.Size(44, 13);
            this.lblStartCounter.TabIndex = 13;
            this.lblStartCounter.Text = "Ö. Değer";
            // 
            // lstPrinterNames
            // 
            this.lstPrinterNames.EnterMoveNextControl = true;
            this.lstPrinterNames.Location = new System.Drawing.Point(64, 42);
            this.lstPrinterNames.MenuManager = this.tabManager;
            this.lstPrinterNames.Name = "lstPrinterNames";
            this.lstPrinterNames.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lstPrinterNames.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lstPrinterNames.Properties.NullText = "Printer Seçiniz";
            this.lstPrinterNames.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.lstPrinterNames.Properties.PopupWidth = 400;
            this.lstPrinterNames.Size = new System.Drawing.Size(136, 20);
            this.lstPrinterNames.TabIndex = 9;
            this.lstPrinterNames.EditValueChanged += new System.EventHandler(this.lstPrinterNames_EditValueChanged);
            // 
            // btnAddCounter
            // 
            this.btnAddCounter.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCounter.Image")));
            this.btnAddCounter.Location = new System.Drawing.Point(64, 146);
            this.btnAddCounter.Name = "btnAddCounter";
            this.btnAddCounter.Size = new System.Drawing.Size(136, 41);
            this.btnAddCounter.TabIndex = 8;
            this.btnAddCounter.Text = "Ekle";
            this.btnAddCounter.Click += new System.EventHandler(this.btnAddCounter_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.EditValue = "";
            this.txtDescription.EnterMoveNextControl = true;
            this.txtDescription.Location = new System.Drawing.Point(64, 120);
            this.txtDescription.MenuManager = this.tabManager;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(136, 20);
            this.txtDescription.TabIndex = 7;
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(8, 123);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(41, 13);
            this.lblDescription.TabIndex = 6;
            this.lblDescription.Text = "Açıklama";
            // 
            // txtCounter
            // 
            this.txtCounter.EditValue = "";
            this.txtCounter.EnterMoveNextControl = true;
            this.txtCounter.Location = new System.Drawing.Point(64, 94);
            this.txtCounter.MenuManager = this.tabManager;
            this.txtCounter.Name = "txtCounter";
            this.txtCounter.Properties.Mask.EditMask = "d";
            this.txtCounter.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCounter.Properties.Mask.ShowPlaceHolders = false;
            this.txtCounter.Size = new System.Drawing.Size(136, 20);
            this.txtCounter.TabIndex = 5;
            // 
            // lblCounter
            // 
            this.lblCounter.Location = new System.Drawing.Point(8, 97);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(29, 13);
            this.lblCounter.TabIndex = 4;
            this.lblCounter.Text = "Sayaç";
            // 
            // lblPrinterNames
            // 
            this.lblPrinterNames.Location = new System.Drawing.Point(8, 45);
            this.lblPrinterNames.Name = "lblPrinterNames";
            this.lblPrinterNames.Size = new System.Drawing.Size(50, 13);
            this.lblPrinterNames.TabIndex = 2;
            this.lblPrinterNames.Text = "Printer Adı";
            // 
            // groupAddPrinter
            // 
            this.groupAddPrinter.CaptionImage = ((System.Drawing.Image)(resources.GetObject("groupAddPrinter.CaptionImage")));
            this.groupAddPrinter.Controls.Add(this.btnPrinterAdd);
            this.groupAddPrinter.Controls.Add(this.txtPrinterLocation);
            this.groupAddPrinter.Controls.Add(this.lblPrinterLocation);
            this.groupAddPrinter.Controls.Add(this.txtPrinterIP);
            this.groupAddPrinter.Controls.Add(this.lblPrinterIP);
            this.groupAddPrinter.Controls.Add(this.txtPrinterName);
            this.groupAddPrinter.Controls.Add(this.lblPrinterName);
            this.groupAddPrinter.Location = new System.Drawing.Point(5, 3);
            this.groupAddPrinter.Name = "groupAddPrinter";
            this.groupAddPrinter.Size = new System.Drawing.Size(214, 174);
            this.groupAddPrinter.TabIndex = 4;
            this.groupAddPrinter.Text = "Printer Ekle";
            // 
            // btnPrinterAdd
            // 
            this.btnPrinterAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnPrinterAdd.Image")));
            this.btnPrinterAdd.Location = new System.Drawing.Point(64, 120);
            this.btnPrinterAdd.Name = "btnPrinterAdd";
            this.btnPrinterAdd.Size = new System.Drawing.Size(136, 45);
            this.btnPrinterAdd.TabIndex = 8;
            this.btnPrinterAdd.Text = "Ekle";
            this.btnPrinterAdd.Click += new System.EventHandler(this.btnPrinterAdd_Click);
            // 
            // txtPrinterLocation
            // 
            this.txtPrinterLocation.EditValue = "";
            this.txtPrinterLocation.EnterMoveNextControl = true;
            this.txtPrinterLocation.Location = new System.Drawing.Point(64, 94);
            this.txtPrinterLocation.MenuManager = this.tabManager;
            this.txtPrinterLocation.Name = "txtPrinterLocation";
            this.txtPrinterLocation.Size = new System.Drawing.Size(136, 20);
            this.txtPrinterLocation.TabIndex = 7;
            // 
            // lblPrinterLocation
            // 
            this.lblPrinterLocation.Location = new System.Drawing.Point(8, 97);
            this.lblPrinterLocation.Name = "lblPrinterLocation";
            this.lblPrinterLocation.Size = new System.Drawing.Size(45, 13);
            this.lblPrinterLocation.TabIndex = 6;
            this.lblPrinterLocation.Text = "Lokasyon";
            // 
            // txtPrinterIP
            // 
            this.txtPrinterIP.EditValue = "";
            this.txtPrinterIP.EnterMoveNextControl = true;
            this.txtPrinterIP.Location = new System.Drawing.Point(64, 68);
            this.txtPrinterIP.MenuManager = this.tabManager;
            this.txtPrinterIP.Name = "txtPrinterIP";
            this.txtPrinterIP.Properties.Mask.BeepOnError = true;
            this.txtPrinterIP.Properties.Mask.EditMask = "([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.([0-9]|[1-9][0-9]|1[0-9][0-9" +
    "]|2[0-4][0-9]|25[0-5])){3}";
            this.txtPrinterIP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrinterIP.Properties.Mask.ShowPlaceHolders = false;
            this.txtPrinterIP.Size = new System.Drawing.Size(136, 20);
            this.txtPrinterIP.TabIndex = 5;
            // 
            // lblPrinterIP
            // 
            this.lblPrinterIP.Location = new System.Drawing.Point(8, 71);
            this.lblPrinterIP.Name = "lblPrinterIP";
            this.lblPrinterIP.Size = new System.Drawing.Size(43, 13);
            this.lblPrinterIP.TabIndex = 4;
            this.lblPrinterIP.Text = "IP Adresi";
            // 
            // txtPrinterName
            // 
            this.txtPrinterName.EditValue = "";
            this.txtPrinterName.EnterMoveNextControl = true;
            this.txtPrinterName.Location = new System.Drawing.Point(64, 42);
            this.txtPrinterName.MenuManager = this.tabManager;
            this.txtPrinterName.Name = "txtPrinterName";
            this.txtPrinterName.Size = new System.Drawing.Size(136, 20);
            this.txtPrinterName.TabIndex = 3;
            // 
            // lblPrinterName
            // 
            this.lblPrinterName.Location = new System.Drawing.Point(8, 45);
            this.lblPrinterName.Name = "lblPrinterName";
            this.lblPrinterName.Size = new System.Drawing.Size(50, 13);
            this.lblPrinterName.TabIndex = 2;
            this.lblPrinterName.Text = "Printer Adı";
            // 
            // actionMenu
            // 
            this.actionMenu.ActiveGroup = this.otherActions;
            this.actionMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionMenu.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.otherActions});
            this.actionMenu.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.btnPrinterList,
            this.btnCounterList});
            this.actionMenu.Location = new System.Drawing.Point(0, 0);
            this.actionMenu.Margin = new System.Windows.Forms.Padding(5);
            this.actionMenu.Name = "actionMenu";
            this.actionMenu.OptionsNavPane.ExpandedWidth = 590;
            this.actionMenu.Size = new System.Drawing.Size(590, 414);
            this.actionMenu.TabIndex = 4;
            this.actionMenu.Text = "navBarControl1";
            // 
            // otherActions
            // 
            this.otherActions.Caption = "Diğer İşlemler";
            this.otherActions.Expanded = true;
            this.otherActions.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnPrinterList),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnCounterList)});
            this.otherActions.LargeImage = ((System.Drawing.Image)(resources.GetObject("otherActions.LargeImage")));
            this.otherActions.Name = "otherActions";
            // 
            // btnPrinterList
            // 
            this.btnPrinterList.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.btnPrinterList.Caption = "Printer Listesi";
            this.btnPrinterList.LargeImageSize = new System.Drawing.Size(32, 32);
            this.btnPrinterList.Name = "btnPrinterList";
            this.btnPrinterList.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPrinterList.SmallImage")));
            this.btnPrinterList.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnPrinterList_LinkClicked);
            // 
            // btnCounterList
            // 
            this.btnCounterList.Caption = "Sayaç Listesi";
            this.btnCounterList.Name = "btnCounterList";
            this.btnCounterList.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnCounterList.SmallImage")));
            this.btnCounterList.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnCounterList_LinkClicked);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 464);
            this.Controls.Add(this.mainScControl);
            this.Controls.Add(this.tabController);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(510, 469);
            this.Name = "frmMain";
            this.TabFormControl = this.tabController;
            this.Text = "Printer Sayaç Takip Sistemi";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabManager)).EndInit();
            this.mainScControl.ResumeLayout(false);
            this.mainScControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupAddCounter)).EndInit();
            this.groupAddCounter.ResumeLayout(false);
            this.groupAddCounter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartCounter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstPrinterNames.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCounter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAddPrinter)).EndInit();
            this.groupAddPrinter.ResumeLayout(false);
            this.groupAddPrinter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinterLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinterIP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinterName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actionMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.TabFormControl tabController;
        private DevExpress.XtraBars.TabFormDefaultManager tabManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.XtraScrollableControl mainScControl;
        private DevExpress.XtraBars.TabFormPage mainTab;
        private DevExpress.XtraBars.SkinBarSubItem themeButton;
        private DevExpress.XtraNavBar.NavBarControl actionMenu;
        private DevExpress.XtraNavBar.NavBarGroup otherActions;
        private DevExpress.XtraNavBar.NavBarItem btnPrinterList;
        private DevExpress.XtraNavBar.NavBarItem btnCounterList;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupAddCounter;
        private DevExpress.XtraEditors.TextEdit txtStartCounter;
        private DevExpress.XtraEditors.LabelControl lblStartCounter;
        private DevExpress.XtraEditors.LookUpEdit lstPrinterNames;
        private DevExpress.XtraEditors.SimpleButton btnAddCounter;
        private DevExpress.XtraEditors.TextEdit txtDescription;
        private DevExpress.XtraEditors.LabelControl lblDescription;
        private DevExpress.XtraEditors.TextEdit txtCounter;
        private DevExpress.XtraEditors.LabelControl lblCounter;
        private DevExpress.XtraEditors.LabelControl lblPrinterNames;
        private DevExpress.XtraEditors.GroupControl groupAddPrinter;
        private DevExpress.XtraEditors.SimpleButton btnPrinterAdd;
        private DevExpress.XtraEditors.TextEdit txtPrinterLocation;
        private DevExpress.XtraEditors.LabelControl lblPrinterLocation;
        private DevExpress.XtraEditors.TextEdit txtPrinterIP;
        private DevExpress.XtraEditors.LabelControl lblPrinterIP;
        private DevExpress.XtraEditors.TextEdit txtPrinterName;
        private DevExpress.XtraEditors.LabelControl lblPrinterName;
    }
}

