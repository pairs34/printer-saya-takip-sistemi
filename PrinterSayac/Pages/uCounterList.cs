﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using PrinterSayac.Entities;

namespace PrinterSayac.Pages
{
    public partial class uCounterList : UserControl
    {
        public uCounterList()
        {
            InitializeComponent();
        }

        private void uCounterList_Load(object sender, EventArgs e)
        {
            LoadCounters();
        }

        private void LoadCounters()
        {
            using (var context = new DbHelper())
            {
                DateTime firstDate = DateTime.Now.FirstDayOfMonth();
                DateTime lastDate = DateTime.Now.LastDayOfMonth();
                var counters = context.Counters
                    .Join(context.Printers, c => c.PrinterID, p => p.ID, (c, p) => new
                    {
                        SayacID = c.ID,
                        PrinterAdı = p.PrinterName,
                        Lokasyon = p.Location,
                        BuAykiSayaç = c.EndCount - c.StartCount,
                        SayaçTarihi = c.CounterDate,
                        Açıklama = c.Description,
                        Aktif = c.IsActive == 1 ? "Aktif" : "Pasif"
                    }).Where(x => x.SayaçTarihi >= firstDate && x.SayaçTarihi <= lastDate && x.Aktif == "Aktif")
                    .ToList();
                var renkli = counters.Where(x => x.PrinterAdı.Contains("Renkli"));
                var siyahBeyaz = counters.Where(x => !x.PrinterAdı.Contains("Renkli"));
                grdCounters.DataSource = counters;
                GridColumnSummaryItem rToplam = new GridColumnSummaryItem(SummaryItemType.Custom, "BuAykiSayaç", "Renkli Toplam=" + renkli.Sum(x => x.BuAykiSayaç));
                GridColumnSummaryItem sbToplam = new GridColumnSummaryItem(SummaryItemType.Custom, "BuAykiSayaç", "SB Toplam=" + siyahBeyaz.Sum(x => x.BuAykiSayaç));
                viewCounters.Columns[3].Summary.Add(rToplam);
                viewCounters.Columns[3].Summary.Add(sbToplam);viewCounters.Columns[3].Width = 150;
            }
        }

        private void btnSayacSil_Click(object sender, EventArgs e)
        {
            int SayacID = (int) viewCounters.GetFocusedRowCellValue(viewCounters.Columns[0]);

            if (SayacID == null)
                return;

            try{
                using (var context = new DbHelper())
                {
                    var RemovedCounter = context.Counters.SingleOrDefault(x => x.ID == SayacID);
                    context.Counters.Remove(RemovedCounter);
                    context.SaveChanges();
                }

                MessageBox.Show("Sayaç silindi");
                LoadCounters();
            }
            catch (Exception err)
            {
                MessageBox.Show("Sayaç silinirken hata oluştu. Hata Kodu : " + err.Message);
            } 
        }

        private void btnPasifYap_Click(object sender, EventArgs e)
        {
            int SayacID = (int)viewCounters.GetFocusedRowCellValue(viewCounters.Columns[0]);

            if (SayacID == null)
                return;

            try
            {
                using (var context = new DbHelper())
                {
                    var RemovedCounter = context.Counters.SingleOrDefault(x => x.ID == SayacID);
                    RemovedCounter.IsActive = 0;
                    context.Counters.AddOrUpdate(RemovedCounter);
                    context.SaveChanges();
                }

                MessageBox.Show("Sayaç pasif edildi");
                LoadCounters();
            }catch (Exception err)
            {
                MessageBox.Show("Sayaç pasif edilirken hata oluştu. Hata Kodu : " + err.Message);
            }
        }
    }
}
