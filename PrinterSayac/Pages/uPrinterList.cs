﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using PrinterSayac.Entities;

namespace PrinterSayac.Pages
{
    public partial class uPrinterList : UserControl
    {
        public uPrinterList()
        {
            InitializeComponent();
        }

        private DbHelper context;
        private void uPrinterList_Load(object sender, EventArgs e)
        {
            LoadPrinters();
        }

        private void LoadPrinters()
        {
            context = new DbHelper();
            var printers = context.Printers.ToList();
            printerDataSource.DataSource = printers;
        }

        void ChangeEditingStates(bool state)
        {
            pnlUIButtons.Buttons[0].Properties.Enabled = state == true ? false : true;
            viewPrinters.OptionsBehavior.Editable = state == false ? false : true;
            pnlUIButtons.Buttons[1].Properties.Enabled = state;
            pnlUIButtons.Buttons[2].Properties.Enabled = state;
        }

        private void viewPrinters_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            ChangeEditingStates(true);
        }

        private void pnlUIButtons_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button.Properties.Tag.ToString() == "btnSave")
            {
                viewPrinters.PostEditor();
                context.SaveChanges();
                context.Dispose();
                ChangeEditingStates(false);
            }
            else if (e.Button.Properties.Tag.ToString() == "btnCancel")
            {
                context.Dispose();
                context = new DbHelper();
                var printers = context.Printers.ToList();
                printerDataSource.DataSource = printers;
                gridPrinters.RefreshDataSource();
                ChangeEditingStates(false);
            }
            else if (e.Button.Properties.Tag.ToString() == "btnEdit"){
                viewPrinters.OptionsBehavior.Editable = true;
                viewPrinters.Columns["ID"].OptionsColumn.AllowEdit = false;
                LoadPrinters();
                ChangeEditingStates(true);
            }
        }
    }
    
}
