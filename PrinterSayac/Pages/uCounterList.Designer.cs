﻿namespace PrinterSayac.Pages
{
    partial class uCounterList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grdCounters = new DevExpress.XtraGrid.GridControl();
            this.sagtik = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSayacSil = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPasifYap = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCounters = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.grdCounters)).BeginInit();
            this.sagtik.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewCounters)).BeginInit();
            this.SuspendLayout();
            // 
            // grdCounters
            // 
            this.grdCounters.ContextMenuStrip = this.sagtik;
            this.grdCounters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCounters.Location = new System.Drawing.Point(0, 0);
            this.grdCounters.MainView = this.viewCounters;
            this.grdCounters.Name = "grdCounters";
            this.grdCounters.Size = new System.Drawing.Size(604, 533);
            this.grdCounters.TabIndex = 0;
            this.grdCounters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewCounters});
            // 
            // sagtik
            // 
            this.sagtik.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSayacSil,
            this.btnPasifYap});
            this.sagtik.Name = "sagtik";
            this.sagtik.Size = new System.Drawing.Size(123, 48);
            // 
            // btnSayacSil
            // 
            this.btnSayacSil.Name = "btnSayacSil";
            this.btnSayacSil.Size = new System.Drawing.Size(122, 22);
            this.btnSayacSil.Text = "Sayacı Sil";
            this.btnSayacSil.Click += new System.EventHandler(this.btnSayacSil_Click);
            // 
            // btnPasifYap
            // 
            this.btnPasifYap.Name = "btnPasifYap";
            this.btnPasifYap.Size = new System.Drawing.Size(122, 22);
            this.btnPasifYap.Text = "Pasif Yap";
            this.btnPasifYap.Click += new System.EventHandler(this.btnPasifYap_Click);
            // 
            // viewCounters
            // 
            this.viewCounters.GridControl = this.grdCounters;
            this.viewCounters.Name = "viewCounters";
            this.viewCounters.OptionsBehavior.Editable = false;
            this.viewCounters.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm;
            this.viewCounters.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            this.viewCounters.OptionsView.ShowFooter = true;
            this.viewCounters.OptionsView.ShowGroupPanel = false;
            // 
            // uCounterList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grdCounters);
            this.Name = "uCounterList";
            this.Size = new System.Drawing.Size(604, 533);
            this.Tag = "Sayaç Listesi";
            this.Load += new System.EventHandler(this.uCounterList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCounters)).EndInit();
            this.sagtik.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.viewCounters)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdCounters;
        private DevExpress.XtraGrid.Views.Grid.GridView viewCounters;
        private System.Windows.Forms.ContextMenuStrip sagtik;
        private System.Windows.Forms.ToolStripMenuItem btnSayacSil;
        private System.Windows.Forms.ToolStripMenuItem btnPasifYap;
    }
}
