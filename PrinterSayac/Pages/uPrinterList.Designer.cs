﻿namespace PrinterSayac.Pages
{
    partial class uPrinterList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uPrinterList));
            this.gridPrinters = new DevExpress.XtraGrid.GridControl();
            this.printerDataSource = new System.Windows.Forms.BindingSource(this.components);
            this.viewPrinters = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pnlButton = new DevExpress.XtraEditors.PanelControl();
            this.pnlUIButtons = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrinters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printerDataSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPrinters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButton)).BeginInit();
            this.pnlButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridPrinters
            // 
            this.gridPrinters.DataSource = this.printerDataSource;
            this.gridPrinters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPrinters.Location = new System.Drawing.Point(0, 74);
            this.gridPrinters.MainView = this.viewPrinters;
            this.gridPrinters.Name = "gridPrinters";
            this.gridPrinters.Size = new System.Drawing.Size(600, 395);
            this.gridPrinters.TabIndex = 0;
            this.gridPrinters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewPrinters});
            // 
            // viewPrinters
            // 
            this.viewPrinters.GridControl = this.gridPrinters;
            this.viewPrinters.Name = "viewPrinters";
            this.viewPrinters.OptionsBehavior.Editable = false;
            this.viewPrinters.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.viewPrinters.OptionsMenu.EnableColumnMenu = false;
            this.viewPrinters.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            this.viewPrinters.OptionsView.ShowGroupPanel = false;
            this.viewPrinters.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.viewPrinters_CellValueChanging);
            // 
            // pnlButton
            // 
            this.pnlButton.Controls.Add(this.pnlUIButtons);
            this.pnlButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButton.Location = new System.Drawing.Point(0, 0);
            this.pnlButton.Name = "pnlButton";
            this.pnlButton.Size = new System.Drawing.Size(600, 74);
            this.pnlButton.TabIndex = 3;
            // 
            // pnlUIButtons
            // 
            this.pnlUIButtons.AllowGlyphSkinning = false;
            this.pnlUIButtons.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Düzenle", ((System.Drawing.Image)(resources.GetObject("pnlUIButtons.Buttons"))), -1, DevExpress.XtraBars.Docking2010.ImageLocation.Default, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", true, -1, true, null, true, false, true, null, "btnEdit", -1, false, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Kaydet", ((System.Drawing.Image)(resources.GetObject("pnlUIButtons.Buttons1"))), -1, DevExpress.XtraBars.Docking2010.ImageLocation.Default, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", true, -1, false, null, true, false, true, null, "btnSave", -1, false, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("İptal", ((System.Drawing.Image)(resources.GetObject("pnlUIButtons.Buttons2"))), -1, DevExpress.XtraBars.Docking2010.ImageLocation.Default, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", true, -1, false, null, true, false, true, null, "btnCancel", -1, false, false)});
            this.pnlUIButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUIButtons.Location = new System.Drawing.Point(2, 2);
            this.pnlUIButtons.Name = "pnlUIButtons";
            this.pnlUIButtons.Size = new System.Drawing.Size(596, 69);
            this.pnlUIButtons.TabIndex = 3;
            this.pnlUIButtons.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.pnlUIButtons_ButtonClick);
            // 
            // uPrinterList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.gridPrinters);
            this.Controls.Add(this.pnlButton);
            this.Name = "uPrinterList";
            this.Size = new System.Drawing.Size(600, 469);
            this.Tag = "Printer Listesi";
            this.Load += new System.EventHandler(this.uPrinterList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPrinters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printerDataSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPrinters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButton)).EndInit();
            this.pnlButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridPrinters;
        private DevExpress.XtraGrid.Views.Grid.GridView viewPrinters;
        private System.Windows.Forms.BindingSource printerDataSource;
        private DevExpress.XtraEditors.PanelControl pnlButton;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel pnlUIButtons;
    }
}
