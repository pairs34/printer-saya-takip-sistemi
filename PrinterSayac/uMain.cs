﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using PrinterSayac.Entities;
using PrinterSayac.Pages;

namespace PrinterSayac
{
    public partial class frmMain : DevExpress.XtraBars.TabForm
    {
        public frmMain()
        {
            InitializeComponent();
        }

        #region Methods

        void ClearAddPrinterTexts()
        {
            txtPrinterIP.Text = string.Empty;
            txtPrinterLocation.Text = string.Empty;
            txtPrinterName.Text = string.Empty;
        }

        void ClearAddCounterTexts()
        {
            lstPrinterNames.EditValue = null;
            lstPrinterNames.ClosePopup();
            txtCounter.Text = string.Empty;
            txtStartCounter.Text = string.Empty;
        }

        void LoadPrinterNames()
        {
            using (var context = new DbHelper())
            {
                var printers = context.Printers.Select(x => new {x.ID,x.PrinterName,x.Location,x.IpAdress}).ToList();

                lstPrinterNames.Properties.DataSource = printers;
                lstPrinterNames.Properties.DisplayMember = "Location";
                lstPrinterNames.Properties.ValueMember = "ID";
            }
        }
        #endregion

        void OnOuterFormCreating(object sender, OuterFormCreatingEventArgs e)
        {
            frmMain form = new frmMain();
            form.TabFormControl.Pages.Clear();
            e.Form = form;
            OpenFormCount++;
        }
        static int OpenFormCount = 1;
        private static UserControl currentForm;
        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadPrinterNames();
        }

        private void btnPrinterAdd_Click(object sender, EventArgs e)
        {
            if (txtPrinterName.Text == "" 
             || txtPrinterIP.Text == "" 
             || txtPrinterLocation.Text == "")
            {
                MessageBox.Show("Tüm alanları doldurmadınız");
                return;
            }
            try
            {
                using (var context = new DbHelper())
                {
                    var printer = new Printer()
                    {
                        PrinterName = txtPrinterName.Text,
                        IpAdress = txtPrinterIP.Text,
                        Location = txtPrinterLocation.Text
                    };

                    context.Printers.Add(printer);context.SaveChanges();
                    ClearAddPrinterTexts();
                    MessageBox.Show("Printer eklendi");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
        private void btnAddCounter_Click(object sender, EventArgs e)
        {
            if (lstPrinterNames.EditValue == null)
            {
                MessageBox.Show("Printer seçmediniz");
                return;
            }else if (txtCounter.Text == "")
            {
                MessageBox.Show("Sayaç girmediniz");
                return;
            }
            try
            {
                using (var context = new DbHelper())
                {
                    DateTime firstDate = DateTime.Now.FirstDayOfMonth();
                    DateTime lastDate = DateTime.Now.LastDayOfMonth();
                    var CounterTotal = context.Counters
                            .Where(x => x.CounterDate >= firstDate && x.CounterDate <= lastDate && x.PrinterID == (int)lstPrinterNames.EditValue)
                            .Count();
                    if (CounterTotal > 0){
                        MessageBox.Show("Bu ay için sayaç girdiniz.Önce sayacı siliniz.");
                        return;
                    }
                    var pCounter = new Counter()
                    {
                        PrinterID = (int)lstPrinterNames.EditValue,
                        CounterDate = DateTime.Now,
                        StartCount = Convert.ToInt32(txtStartCounter.Text),
                        EndCount = Convert.ToInt32(txtCounter.Text),
                        Description = txtDescription.Text,
                        IsActive = 1
                    };
                    context.Counters.Add(pCounter);
                    context.SaveChanges();
                    ClearAddCounterTexts();
                    MessageBox.Show("Sayaç eklendi");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btnPrinterList_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            currentForm = new uPrinterList();
            tabController.AddNewPage();
        }

        private void tabController_PageCreated(object sender, PageCreatedEventArgs e)
        {
            currentForm.Dock = DockStyle.Fill;
            e.Page.ContentContainer.Controls.Add(currentForm);
            e.Page.Text = currentForm.Tag.ToString();
        }

        private void tabController_PageClosing(object sender, PageClosingEventArgs e)
        {
            foreach (Control contrl in e.Page.ContentContainer.Controls)
            {
                if (contrl.GetType().BaseType == typeof(UserControl))
                    contrl.Dispose();
            }
        }

        private void btnCounterList_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            currentForm = new uCounterList();
            tabController.AddNewPage();
        }

        private void lstPrinterNames_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                using (var context = new DbHelper())
                {
                    var firstDate = DateTime.Now.PreviousMonthFirstDay();
                    var lastDate = DateTime.Now.PreviousMonthLastDay();
                    var StartCounter = context.Counters.Where(x => x.PrinterID == (int) lstPrinterNames.EditValue)
                        .Where(x => x.CounterDate >= firstDate && x.CounterDate <= lastDate && x.IsActive == 1)
                        .Select(x => new {x.EndCount}).Single();
                    txtStartCounter.Text = StartCounter.EndCount.ToString();
                }
            }
            catch (Exception)
            {
                txtStartCounter.Text = "0";
            }
        }
    }
}
