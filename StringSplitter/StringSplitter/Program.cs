﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringSplitter
{
    class Program
    {
        static void Main(string[] args)
        {
            string pdf = File.ReadAllText(Application.StartupPath + "\\test.txt");

            List<string> cumleler = new List<string>();
            List<string> tamamlanan = new List<string>();
            cumleler.AddRange(pdf.Split('.'));
            string yazi = String.Empty;
            for (int i = 0; i < cumleler.Count; i++)
            {
                yazi += cumleler[i];
                if (yazi.Length >= 200)
                {
                    tamamlanan.Add(yazi);
                    yazi = string.Empty;
                }
            }

            foreach (var str in tamamlanan)
            {
               Console.WriteLine("\r\n" + str);
            }
            Console.Read();
        }
    }
}
